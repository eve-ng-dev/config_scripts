#!/bin/bash
cp $1/startup-config $1/iosxe_config.txt
mkisofs -o $1/config.iso -l --iso-level 2 $1/iosxe_config.txt
